import { controls } from '../../constants/controls';

export async function fight(firstFighter, secondFighter) {
  
  
  return new Promise((resolve) => {
    // resolve the promise with the winner when fight is over


    let leftHealth = firstFighter.health;
    let rightHealth = secondFighter.health;
    
    let percentage = 100;

    let firstBlock = false;
    let secondBlock = false;

    let firstCooldown = true;
    let secondCooldown = true;

    document.addEventListener('keydown', (e)=>{
      if(e.code == controls.PlayerOneBlock){
        firstBlock = true;
      }
      if(e.code == controls.PlayerTwoBlock){
        secondBlock = true;
      }
    });

    document.addEventListener('keyup', (e)=>{
      if(e.code == controls.PlayerOneBlock){
        firstBlock = false;
      }
      if(e.code == controls.PlayerTwoBlock){
        secondBlock = false;
      }
    });

    document.addEventListener('keyup', (event) => {
      if (event.code == controls.PlayerTwoAttack && !firstBlock && !secondBlock) {
        leftHealth -= getDamage(secondFighter, firstFighter);
        const leftBar = document.getElementById('left-fighter-indicator');
        let percentage = leftHealth * 100 / firstFighter.health;
        if (percentage < 0) {
          leftBar.style.width = '0';
          resolve(secondFighter);
        }

        leftBar.style.width = `${percentage}%`;

      }
      else if (event.code == controls.PlayerOneAttack && !secondBlock && !firstBlock){
        rightHealth -= getDamage(firstFighter, secondFighter);
        const rightBar = document.getElementById('right-fighter-indicator');
        let percentage = rightHealth * 100 / secondFighter.health;
        if (percentage < 0) {
          rightBar.style.width = '0'; 
          resolve(firstFighter);
      }
        rightBar.style.width = `${percentage}%`;
      }
  });



  ultimate(()=>{
    if (firstCooldown && !firstBlock) {
      const rightBar = document.getElementById('right-fighter-indicator');
      rightHealth -= ultiDamage(firstFighter);
      let percentage = rightHealth * 100 / secondFighter.health;
      if (percentage < 0) {
        rightBar.style.width = '0'; 
        resolve(firstFighter);
      }
      rightBar.style.width = `${percentage}%`;
      firstCooldown = false;
      setTimeout(()=>{firstCooldown=true;}, 10000)
    } 

  }, ...controls.PlayerOneCriticalHitCombination)

  ultimate(()=>{
    if (secondCooldown && !secondBlock){
      const leftBar = document.getElementById('left-fighter-indicator');
      leftHealth -= ultiDamage(secondFighter)
      let percentage = leftHealth * 100 / firstFighter.health;
          if (percentage < 0) {
            leftBar.style.width = '0'; 
            resolve(secondFighter);
        }
      leftBar.style.width = `${percentage}%`;
      secondCooldown = false;
      setTimeout(()=>{firstCooldown=true;}, 10000)  
    }
  }, ...controls.PlayerTwoCriticalHitCombination)




  });
}


function ultimate(func, ...codes) {
  let pressed = new Set();

      document.addEventListener('keydown', function(event) {
        pressed.add(event.code);

        for (let code of codes) { 
          if (!pressed.has(code)) {
            return;
          }
        }
        pressed.clear();

        func();
      });

      document.addEventListener('keyup', function(event) {
        pressed.delete(event.code);
      });
}


function ultiDamage(fighter) {
  return fighter.attack * 2;
}

export function getDamage(attacker, defender) {
  const power = getHitPower(attacker) - getBlockPower(defender)
  if (power < 0) return 0;
  else return power;

}

function criticalHitChance() {
  const max = 2, min = 1;

  return Math.random() * (max - min) + min;
}

export function getHitPower(fighter) {
  return fighter.attack * criticalHitChance();
}
function dodgeChance() {
  const max = 2, min = 1;

  return Math.random() * (max - min) + min;
}
export function getBlockPower(fighter) {
  return fighter.defense * dodgeChance();
}
