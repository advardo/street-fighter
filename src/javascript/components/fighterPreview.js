import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  // todo: show fighter info (image, name, health, etc.)
  const {name, health, attack, defense, source} = fighter;
  const attributes = {
    src: source, 
    title: name,
    alt: name 
  }
  const fighterImg = createElement({
    tagName: 'img',
    className: `prev_img`,
    attributes,
  });
  
  const fighterDesc = createElement({
    tagName: 'span',
    className: 'prev_desc',
  })
  fighterDesc.innerHTML = `${name}<br>Health: ${health}<br>Attack: ${attack}<br>Defense: ${defense}\n`
  fighterElement.append(fighterImg, fighterDesc);
  return fighterElement;
}


export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = { 
    src: source, 
    title: name,
    alt: name 
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
