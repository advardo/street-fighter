import { createElement } from '../../helpers/domHelper';
import {showModal} from './modal';
import App from './../../app'
export function showWinnerModal(fighter) {
  // call showModal function 
  const title = `${fighter.name} wins!`;

 

  const bodyElement = createElement({
    tagName: 'div',
    className: 'button-again',
  });
  bodyElement.innerHTML = 'Take a revenge';
  function onClose() {
    root.innerHTML='';
    new App;
  };
  

  showModal({title, bodyElement, onClose });
}
